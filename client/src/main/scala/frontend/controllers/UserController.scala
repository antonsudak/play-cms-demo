package frontend.controllers

import autowire._
import boopickle.Default._
import frontend.services.AjaxClient
import org.scalajs.dom._
import shared.Api

import scala.concurrent.ExecutionContext.Implicits.global

object  UserController {

  /**
    * Logs server Users to console (This method only exists for educational purposes)
    */
  def logUsers(): Unit = {
    AjaxClient[Api].getUsers().call().map(
      (users) => console.log(users.toString)
    )
  }

}
