import com.typesafe.sbt.SbtNativePackager.autoImport._
import sbt.Project.projectToRef

scalaVersion := Settings.versions.scala

lazy val server = (project in file("server"))
  .settings(
    name := Settings.name,
    organization := Settings.organization,
    version := Settings.version,
    scalaVersion := Settings.versions.scala,
    scalacOptions ++= Settings.scalacOptions,
    libraryDependencies ++= Settings.jvmDependencies.value,
    // connect to the client project
    scalaJSProjects := Seq(client),
    pipelineStages in Assets := Seq(scalaJSPipeline),
    pipelineStages := Seq(uglify, digest, gzip),
    compile in Compile := ((compile in Compile) dependsOn scalaJSPipeline).value,

    resolvers ++= Settings.resolvers.value,

    // a simple test support of non-dockerized application(for the usage of sbt:test outside of docker)
    javaOptions in Test += "-Dconfig.file=conf/application.test.conf",

    // fixes problem with loading entities in prod mode,
    // see: https://github.com/playframework/playframework/issues/4590
    PlayKeys.externalizeResources := false,

    // Fixes: compilation error "File name too long" which can happen on some encrypted or legacy file systems.
    // Please see [SI-3623](https://issues.scala-lang.org/browse/SI-3623) for more details.
    scalacOptions ++= Seq("-Xmax-classfile-name","100"),

    // Root Path for included CSS in main.less
    LessKeys.rootpath := "/",
    // Adjust Urls imported in main.less
    LessKeys.relativeUrls := true,
    // compress CSS
    LessKeys.compress in Assets := true
  )
  .enablePlugins(PlayJava, SbtWeb)
  .dependsOn(sharedJvm)
  .devModules(
    "ch.insign" %% "play-cms" % Settings.playCmsVersion as "cms" at "modules/play-cms" when Settings.playCmsLocal,
    "ch.insign" %% "play-auth" % Settings.playCmsVersion as "auth" at "modules/play-cms" when Settings.playCmsLocal,
    "ch.insign" %% "play-commons" % Settings.playCmsVersion as "commons" at "modules/play-cms" when Settings.playCmsLocal,
    "ch.insign" %% "play-theme-metronic" % Settings.playCmsVersion as "metronic" at "modules/play-cms" when Settings.playCmsLocal)

//////////////////////////
// ScalaJs Integration
//////////////////////////

// This demo-project uses Scala-js in the frontend. Scala-js Projects are usually grouped into three individual projects
// shared, client and server (the actual play-application).
// if you want to write frontend-script in Javascript, you can remove all code in this Section, but you have to make sure, that you also get rid of the following things:
// - remove scala-js-related project-folders: shared and client. you are left with server, which is your actual play-appliation
// - remove scala-js plugin from plugins.sbt
// - remove scala-js relevant settings from server-project (above)
// - get rid of scala-js includes in main.scala.html template and use conventional javascript-includes instead
// - remove sample-implementation of Autowire-API as provided in Application, at the following two places: Application.scala::autowireApi and ApiService.scala
// - remove dependencies for unused project in project/Settings.scala

// instantiate the scala-js project
lazy val client: Project = (project in file("client"))
  .settings(
    name := "client",
    version := Settings.version,
    scalaVersion := Settings.versions.scala,
    scalacOptions ++= Settings.scalacOptions,
    libraryDependencies ++= Settings.scalajsDependencies.value,
    // by default we do development build, no eliding
    jsDependencies ++= Settings.jsDependencies.value,
    // RuntimeDOM is needed for tests
    jsDependencies += RuntimeDOM % "test",
    // yes, we want to package JS dependencies
    skip in packageJSDependencies := false,
    // use Scala.js provided launcher code to start the client app
    scalaJSUseMainModuleInitializer := true,
    scalaJSUseMainModuleInitializer in Compile := true,
    scalaJSUseMainModuleInitializer in Test := false,
    // use uTest framework for tests
    testFrameworks += new TestFramework("utest.runner.Framework"),
    resolvers ++= Settings.resolvers.value
  )
  .enablePlugins(ScalaJSPlugin, ScalaJSWeb)
  .dependsOn(sharedJs)

// crossProject for configuring a JS/JVM/shared structure
lazy val shared = (crossProject.crossType(CrossType.Pure) in file("shared"))
  .settings(
    scalaVersion := Settings.versions.scala,
    libraryDependencies ++= Settings.sharedDependencies.value,
    resolvers ++= Settings.resolvers.value
  )
  .jsConfigure(_ enablePlugins ScalaJSWeb)

lazy val sharedJvm = shared.jvm
lazy val sharedJs = shared.js

//////////////////////////
// End of ScalaJs Integration
//////////////////////////

enablePlugins(DockerPlugin)
maintainer in Docker := "Martin Bachmann <m.bachmann@insign.ch>"
packageSummary in Docker := "The play-cms demo application."
aggregate in Docker := false
dockerRepository := Some("insign-docker-registry.bintray.io")

// Fixes: compilation error "File name too long" which can happen on some encrypted or legacy file systems.
// Please see [SI-3623](https://issues.scala-lang.org/browse/SI-3623) for more details.
scalacOptions ++= Seq("-Xmax-classfile-name","100")

// Resolve only newly added dependencies
updateOptions := updateOptions.value.withCachedResolution(true)

// loads the Play server project at sbt startup
onLoad in Global := (Command.process("project server", _: State)) compose (onLoad in Global).value
