#!/usr/bin/env bash

set -e

if [ "$1" = 'sbt' ]; then
    # dev mode

    # Make sure `sbt` user has the same uid as owner of the workdir
    usermod -u `stat -c "%u" /var/app/current` sbt
    chown -R sbt:sbt /var/app/current
    chown -R sbt:sbt /home/sbt
    export HOME=/home/sbt

    exec gosu sbt "$@"
else
    exec "$@"
fi
