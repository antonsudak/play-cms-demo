#!/usr/bin/env bash

set -e

if [ -n "$DOCKER_HOST_VOLUME_UID" ]; then
    usermod -u $DOCKER_HOST_VOLUME_UID www-data
    chown -R www-data:www-data /var/lock/apache2 /var/run/apache2 /var/log/apache2 /var/www/html
fi

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- apache2-foreground "$@"
fi

exec "$@"