#!/bin/bash

COMPOSE_PROJECT_NAME=playcmsdemo
export COMPOSE_PROJECT_NAME

PROJECT_ABS_PATH="$(cd $(dirname $0)/.. && pwd -P)"
export PROJECT_ABS_PATH


function resolveDockerHostVolumeUid() {
    if [[ -z "$DOCKER_HOST_VOLUME_UID" ]]; then
        if [ "$(uname)" == "Darwin" ]; then
            STAT="stat -f"
        elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
            STAT="stat -c"
        fi

        if [[ -z "$DOCKER_MACHINE_NAME" && -n "$STAT" ]]; then
            DOCKER_HOST_VOLUME_UID=`${STAT} "%u" $PROJECT_ABS_PATH`
        else
            DOCKER_HOST_VOLUME_UID=""
        fi
    fi

    echo $DOCKER_HOST_VOLUME_UID
}

function resolveDockerHostAddr() {
    # Resolve the address of the docker server
    if [[ -z "$DOCKER_HOST_ADDR" ]]; then
        if [[ -n "$DOCKER_MACHINE_NAME" ]]; then
            DOCKER_HOST_ADDR=`docker-machine ip $DOCKER_MACHINE_NAME`
        else
            DOCKER_HOST_ADDR="127.0.0.1"
        fi
    fi

    echo $DOCKER_HOST_ADDR
}

function resolveDockerClientHostAddr() {
    # Resolve the address of the docker client as seen from docker containers
    if [[ -z "$DOCKER_MACHINE_NAME" ]]; then
        # If docker server runs locally then local host services can be accessed via gateway address of the default
        # docker "bridge" network which is always "172.17.0.1"
        DOCKER_CLIENT_HOST_ADDR="172.17.0.1"
    else
        # If docker server runs on the remote host then local host can be accessed via LAN address
        # which is usually has a route to the Internet (or Google's DNS)
        DOCKER_CLIENT_HOST_ADDR=`ip route get 8.8.8.8 | tr -d '\n' | sed -e 's/^.* src \([^ ]*\).*$/\1/'`
    fi

    echo $DOCKER_CLIENT_HOST_ADDR
}

function services_yml() {
    if [[ -z "$COMPOSE_FILE" ]]; then
        COMPOSE_FILE="${PROJECT_ABS_PATH}/devops/docker/services.yml"
    fi

    if [[ -z "$DOCKER_HOST_VOLUME_UID" ]]; then
        DOCKER_HOST_VOLUME_UID="$(resolveDockerHostVolumeUid)"
    fi

    if [[ -z "$DOCKER_HOST_ADDR" ]]; then
        DOCKER_HOST_ADDR="$(resolveDockerHostAddr)"
    fi

    if [[ -z "$DOCKER_CLIENT_HOST_ADDR" ]]; then
        DOCKER_CLIENT_HOST_ADDR="$(resolveDockerClientHostAddr)"
    fi

    if [[ -z "$DOCKER_HOST_NAME" ]]; then
        DOCKER_HOST_NAME=$(dig +short -x $DOCKER_HOST_ADDR | sed 's/.$//')
        DOCKER_HOST_NAME=${DOCKER_HOST_NAME:-$DOCKER_HOST_ADDR}
    fi

    (cd $(dirname $0) && \
    DOCKER_HOST_VOLUME_UID="${DOCKER_HOST_VOLUME_UID}" \
    DOCKER_HOST_NAME="${DOCKER_HOST_NAME}" \
    DOCKER_HOST_ADDR="${DOCKER_HOST_ADDR}" \
    PROJECT_ABS_PATH="${PROJECT_ABS_PATH}" \
    DOCKER_CLIENT_HOST_ADDR="${DOCKER_CLIENT_HOST_ADDR}" \
       docker-compose -p ${COMPOSE_PROJECT_NAME} -f ${COMPOSE_FILE} "$@")
}

function sbt_yml() {
    COMPOSE_FILE="${PROJECT_ABS_PATH}/devops/docker/sbt.yml" \
        services_yml "$@"
}

function export_app_env() {
    # Read environment variables from the 'app' service

    sbt_yml up -d --no-deps app && sbt_yml kill app > /dev/null 2>&1

    APP_SERVICE="${COMPOSE_PROJECT_NAME}_app_1"
    APP_ENV=$(docker inspect \
        -f '{{range $index, $value := .Config.Env}}export "{{$value}}"{{println}}{{end}}' ${APP_SERVICE} \
        | grep -E "(APPLICATION_*|MYSQL_*|SBT_OPTS*|SMTP_*|UPLOADS_*)")

    echo ${APP_ENV}
}
