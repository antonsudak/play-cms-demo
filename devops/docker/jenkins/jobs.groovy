import jenkins.model.*

new File("/var/jenkins_home/templates").eachFile() { file ->
    def jobName = file.name.replaceFirst(~/\.[^\.]+$/, '')
    def configXml  = new ByteArrayInputStream(file.getText("UTF-8").getBytes())

    job = Jenkins.instance.getItemByFullName(jobName)
    if (job == null) {
        Jenkins.instance.createProjectFromXML(jobName, configXml)
    }
}