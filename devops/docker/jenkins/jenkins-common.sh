#!/usr/bin/env bash

BASE_PATH=$(dirname $0)

function jenkins_cli() {
    if [[ -z "$JENKINS_HOST" ]]; then
        if [[ -n "$DOCKER_MACHINE_NAME" ]]; then
            JENKINS_HOST=`docker-machine ip $DOCKER_MACHINE_NAME`
        else
            JENKINS_HOST="localhost"
        fi
    fi

    if [[ -z "$JENKINS_PORT" ]]; then
        JENKINS_PORT="8080"
    fi

    if [[ ! -f "$BASE_PATH/jenkins-cli.jar" ]]; then
        (cd $BASE_PATH && curl -O http://$JENKINS_HOST:$JENKINS_PORT/jnlpJars/jenkins-cli.jar)
    fi

    (cd $BASE_PATH && java -jar "jenkins-cli.jar" -s "http://$JENKINS_HOST:$JENKINS_PORT/" "$@")
}