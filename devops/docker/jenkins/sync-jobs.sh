#!/usr/bin/env bash

BASE_PATH=$(dirname $0)

source ${BASE_PATH}/jenkins-common.sh

jobs=$(jenkins_cli list-jobs | xargs)
for job in $jobs; do
     echo "Syncing $job job..."
    jenkins_cli get-job "$job" > "$BASE_PATH/jobs/$job.xml";
done
