import jenkins.model.*
import hudson.security.*

def instance = Jenkins.getInstance()
def realm = instance.getSecurityRealm()

if ( ! (realm instanceof HudsonPrivateSecurityRealm)) {
    // enable security
    realm = new HudsonPrivateSecurityRealm(false)
    instance.setSecurityRealm(realm)
}