#!/usr/bin/env bash

# This script is intended to extract string "artifactId:version\n"
# from pom.properties of a specific jenkins plugin.

if [ -f "$1" ]; then
    source "$1"
    printf "${artifactId}:${version}\n"
fi