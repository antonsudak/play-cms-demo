import jenkins.model.*

def sbtDesc = Jenkins.instance.getDescriptorByType(org.jvnet.hudson.plugins.SbtPluginBuilder.DescriptorImpl.class)
def sbtInst = new org.jvnet.hudson.plugins.SbtPluginBuilder.SbtInstallation("SBT", "/usr/share/sbt-launcher-packaging/bin/sbt-launch.jar", "", [])
sbtDesc.setInstallations(sbtInst)

def nodejsDesc = Jenkins.instance.getDescriptorByType(jenkins.plugins.nodejs.tools.NodeJSInstallation.DescriptorImpl.class)
def nodejsInst = new jenkins.plugins.nodejs.tools.NodeJSInstallation("NodeJS", "/usr/local", [])
nodejsDesc.setInstallations(nodejsInst)
nodejsDesc.save()