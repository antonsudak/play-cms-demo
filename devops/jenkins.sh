#!/usr/bin/env bash

BASE_PATH=$(dirname $0)

if [[ -z "$JENKINS_PORT" ]]; then
    JENKINS_PORT="8080"
fi

if [[ -z "$JENKINS_SLAVE_PORT" ]]; then
    JENKINS_SLAVE_PORT="50000"
fi

if [[ -n "$@" ]]; then
    CMD="$@"
else
    CMD="up -d"
    echo "Starting jenkins on port $JENKINS_PORT..."
fi

JENKINS_PORT=$JENKINS_PORT \
JENKINS_SLAVE_PORT=$JENKINS_SLAVE_PORT \
    docker-compose -p "playcmsdemo-ci" -f ${BASE_PATH}/docker/jenkins.yml $CMD
