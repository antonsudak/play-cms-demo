#!/usr/bin/env bash

BASE_PATH=$(dirname $0)
source ${BASE_PATH}/docker/common.sh

## USAGE
#
# Run services.yml:
#
#   services.sh [docker-composee args...]
#
#
# Run sbt.yml:
#
#   services.sh sbt [sbt args...] [-Dplaycms.version=x.y.z|local]
##

function help() {
cat <<EOF

docker-compose wrapper with convenient defaults and useful additions.

Usage:

  $0 [help]                         Show this help
  $0 [docker-composee options...]   Run docker-compose command
  $0 sbt [sbt options...]           Run sbt command

Examples:

  $0 up           Run services.yml
  $0 sbt run      Run sbt.yml and local sbt
  $0 logs db      Display "db" service logs

EOF
}

for i in "$@"; do
case $i in
    sbt)
    IS_SBT_CMD="YES" || IS_SBT_CMD="NO"
    shift
    ;;

    sbtyml)
    IS_SBTYML_CMD="YES" || IS_SBTYML_CMD="NO"
    shift
    ;;

    help)
    help
    exit
    shift
    ;;

    *)
        CMD="${CMD} ${i}"
    ;;
esac
done


if [[ "$IS_SBT_CMD" == "YES" ]]; then
    echo "Executing command: sbt ${CMD}"

    sbt_yml up -d --no-deps frontproxy
    sbt_yml up -d --no-deps proxy
    sbt_yml up -d --no-deps db
    sbt_yml up -d --no-deps smtp
    sbt_yml up -d --no-deps phpmyadmin

    HOST_SBT_OPTS=${SBT_OPTS}
    eval $(export_app_env)

    (cd ${PROJECT_ABS_PATH} && env -i \
        UPLOADS_PATH="${UPLOADS_PATH}" \
        APPLICATION_HOST="${APPLICATION_HOST}" \
        APPLICATION_NAME="${APPLICATION_NAME}" \
        MYSQL_HOST="${DOCKER_HOST_ADDR}" \
        MYSQL_PORT="${MYSQL_PORT}" \
        MYSQL_DATABASE="${MYSQL_DATABASE}" \
        MYSQL_USER="${MYSQL_USER}" \
        MYSQL_PASSWORD="${MYSQL_PASSWORD}" \
        MYSQL_ROOT_PASSWORD="${MYSQL_ROOT_PASSWORD}" \
        SMTP_HOST="${DOCKER_HOST_ADDR}" \
        SMTP_PORT="${SMTP_PORT}" \
        HOME="${HOME}" \
        PATH="${PATH}" \
        SBT_OPTS="${SBT_OPTS} ${HOST_SBT_OPTS}" \
        sbt -d ${CMD})

    sbt_yml stop
elif [[ "$IS_SBTYML_CMD" == "YES" ]]; then
    echo "Executing command: docker-compose -f sbt.yml... ${CMD}"
    sbt_yml ${CMD}
elif [[ -n $CMD ]]; then
    echo "Executing command: docker-compose -f services.yml... ${CMD}"
    services_yml ${CMD}
else
    help
fi
