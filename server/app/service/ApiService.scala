package service

import java.util.function.Supplier
import javax.inject._

import ch.insign.playauth.PlayAuthApi
import play.db.jpa.{JPA, JPAApi}
import shared.{Api, UserItem}

import scala.collection.JavaConversions._

class ApiService @Inject() (jPAApi: JPAApi, playAuth: PlayAuthApi) extends Api {

  // message of the day
  override def simpleAjaxCall(name: String): String = "this is a stupid string originating from server"

  // get User items
  override def getUsers: Seq[UserItem] = {
    jPAApi.withTransaction(new Supplier[Seq[UserItem]]{
      override def get(): Seq[UserItem] = {
        val users = playAuth.getPartyManager.findAll
        users.map(u => UserItem(u.getId, u.getName, u.getEmail)).toSeq
      }
    })
  }

}
