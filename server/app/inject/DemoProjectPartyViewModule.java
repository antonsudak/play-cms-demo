package inject;

import ch.insign.cms.models.party.PartyEvents;
import ch.insign.cms.models.party.view.*;
import ch.insign.cms.models.party.view.support.*;
import party.DemoPartyHandler;
import party.view.DemoPartyChangePasswordView;
import party.view.DemoPartyCreateView;
import party.view.DemoPartyEditView;
import party.view.DemoPartyProfileView;
import play.api.Configuration;
import play.api.Environment;
import play.api.inject.Binding;
import scala.collection.Seq;

public class DemoProjectPartyViewModule extends play.api.inject.Module {
    @Override
    public Seq<Binding<?>> bindings(Environment environment, Configuration configuration) {
        return seq(
                bind(PartyEvents.class).to(DemoPartyHandler.class),
                bind(PartyEditView.class).to(DemoPartyEditView.class),
                bind(PartyMenuItemsView.class).to(DefaultPartyMenuItemsView.class),
                bind(PartyListView.class).to(DefaultPartyListView.class),
                bind(PartyCreateView.class).to(DemoPartyCreateView.class),
                bind(PartyChangePasswordView.class).to(DemoPartyChangePasswordView.class),
                bind(PartyProfileView.class).to(DemoPartyProfileView.class),
                bind(PartyEditRolesView.class).to(DefaultPartyEditRolesView.class));
    }
}
