package inject;

import ch.insign.playauth.party.PartyManager;
import party.DemoPartyManager;
import play.api.Configuration;
import play.api.Environment;
import play.api.inject.Binding;
import play.api.inject.Module;
import scala.collection.Seq;

public class DemoProjectPartyManagerModule extends Module {
    public Seq<Binding<?>> bindings(Environment environment, Configuration configuration) {
        return seq(bind(PartyManager.class).to(DemoPartyManager.class));
    }
}
