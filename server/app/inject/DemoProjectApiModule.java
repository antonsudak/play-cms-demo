package inject;

import ch.insign.cms.CMSApi;
import ch.insign.cms.CMSApiImpl;
import ch.insign.cms.CMSApiLifecycle;
import ch.insign.cms.events.BlockEventHandlerProvider;
import ch.insign.cms.events.BlockEventHandlerProviderImpl;
import util.DemoProjectCmsApiLifecycle;
import play.api.Configuration;
import play.api.Environment;
import play.api.inject.Binding;
import scala.collection.Seq;

public class DemoProjectApiModule extends play.api.inject.Module {
    @Override
    public Seq<Binding<?>> bindings(Environment environment, Configuration configuration) {
        return seq(
                bind(BlockEventHandlerProvider.class).to(BlockEventHandlerProviderImpl.class),
                bind(CMSApiLifecycle.class).to(DemoProjectCmsApiLifecycle.class),
                bind(CMSApi.class).to(CMSApiImpl.class));
    }
}
