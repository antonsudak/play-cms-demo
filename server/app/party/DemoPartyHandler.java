package party;

import ch.insign.cms.email.EmailService;
import ch.insign.cms.models.CMS;
import ch.insign.cms.models.party.DefaultPartyHandler;
import ch.insign.playauth.PlayAuthApi;
import ch.insign.playauth.party.support.DefaultParty;
import ch.insign.playauth.party.support.DefaultPartyRole;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import play.Application;
import play.data.Form;
import play.mvc.Http;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

import static data.form.RegisterUserForm.PHONE_PATTERN;

public class DemoPartyHandler extends DefaultPartyHandler {
    private final PlayAuthApi playAuthApi;
    private final EmailService emailService;
    private Provider<Application> application;

    @Inject
    public DemoPartyHandler(
            PlayAuthApi playAuthApi,
            EmailService emailService,
            Provider<Application> application
    ) {
        this.playAuthApi = playAuthApi;
        this.emailService = emailService;
        this.application = application;
    }

    @Override
    public void onCreate(Form form, DefaultParty party) {
        User user = (User) party;
        User userForm = (User) form.get();

        user.setGender(userForm.getGender());
        if (!StringUtils.isBlank(userForm.getPhone()) && Pattern.compile(PHONE_PATTERN).matcher(userForm.getPhone()).find()) {
            user.setPhone(userForm.getPhone());
        }
        if (CMS.getConfig().frontendLanguages().stream().anyMatch(l -> l.equals(userForm.getLanguage()))) {
            user.setLanguage(userForm.getLanguage());
        }
        user.setFirstName(userForm.getFirstName());
        user.setLastName(userForm.getLastName());
        user.setName((user.getFirstName() + " " + user.getLastName()).trim());
        if (!StringUtils.isBlank(userForm.getImage())) setFileName(user, userForm);
        user.addRole(playAuthApi.getPartyRoleManager().findOneByName(DefaultPartyRole.ROLE_USER));
    }

    @Override
    public void onUpdate(Form form, DefaultParty party) {
        User user = (User) party;
        User userForm = (User) form.get();
        user.setName((user.getFirstName() + " " + user.getLastName()).trim());
        if (!StringUtils.isBlank(userForm.getPhone()) && Pattern.compile(PHONE_PATTERN).matcher(userForm.getPhone()).find()) {
            user.setPhone(userForm.getPhone());
        }
        if (CMS.getConfig().frontendLanguages().stream().anyMatch(l -> l.equals(userForm.getLanguage()))) {
            user.setLanguage(userForm.getLanguage());
        }
        if (!StringUtils.isBlank(userForm.getImage())) setFileName(user, userForm);
        else {
            user.setImage(null);
            deletePreviouseFile(user);
        }
    }

    @Override
    public void onPasswordUpdate(Form form, DefaultParty party) {
        User user = (User) party;
        HashMap<String, String> emailData = new HashMap<>();
        emailData.put("name", user.getName());

        emailService.send("password.recovery.success", user.getEmail(), emailData, Http.Context.current().lang().language());
    }

    private void setFileName(User user, User userForm) {
        String appPath = application.get().path().getPath();
        try {
            File tmpFile = FileUtils.getFile(appPath + File.separator + userForm.getImage());
            File userAvatar = new File(appPath + CMS.getConfig().imageUploadRootPath() + File.separator +
                    (new Date()).getTime() + user.getId());
            FileUtils.copyFile(tmpFile, userAvatar);
            user.setImage(userAvatar.getName());
            tmpFile.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deletePreviouseFile(User user) {
        String appPath = application.get().path().getPath();
        try {
            File folder = new File(appPath + CMS.getConfig().imageUploadRootPath());
            File[] listOfFiles = folder.listFiles();

            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile() && listOfFiles[i].getName().endsWith(user.getId())) {
                    listOfFiles[i].delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
