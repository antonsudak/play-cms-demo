package party;

import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.support.DefaultPartyManager;
import com.google.inject.Inject;
import play.db.jpa.JPAApi;

public class DemoPartyManager extends DefaultPartyManager {

    @Inject
    public DemoPartyManager(JPAApi jpaApi) {
        super(jpaApi);
    }

    @Override
    public Class<? extends Party> getPartyClass() {
        return User.class;
    }
}
