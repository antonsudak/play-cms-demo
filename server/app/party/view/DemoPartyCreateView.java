package party.view;

import ch.insign.cms.models.party.view.support.DefaultPartyCreateView;
import play.twirl.api.Html;

public class DemoPartyCreateView extends DefaultPartyCreateView {
    @Override
    public Html render() {
        return views.html.admin.user.createForm.render(partyForm);
    }
}
