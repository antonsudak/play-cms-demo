package party.view;

import ch.insign.cms.models.party.view.PartyMenuItemsView;
import ch.insign.cms.models.party.view.support.DefaultPartyEditView;
import com.google.inject.Inject;
import play.twirl.api.Html;

public class DemoPartyEditView extends DefaultPartyEditView {

    @Inject
    public DemoPartyEditView(PartyMenuItemsView partyMenuItemsView) {
        super(partyMenuItemsView);
    }

    @Override
    public Html render() {
        return views.html.admin.user.editForm.render(
            partyForm,
            party
        );
    }
}
