package party;

import ch.insign.playauth.party.ISOGender;
import ch.insign.playauth.party.support.DefaultParty;
import org.apache.commons.lang3.StringUtils;
import play.data.validation.ValidationError;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static data.form.RegisterUserForm.PASSWORD_PATTERN;

/**
 * Demo user class extends DefaultParty with custom fields
 */
@Entity
public class User extends DefaultParty {
    private String firstName;

    private String lastName;

    private String image;

    @Enumerated(EnumType.STRING)
    public ISOGender gender;

    public String phone;

    public String language;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public ISOGender getGender() {
        return gender;
    }

    public void setGender(ISOGender gender) {
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String getName() {
        return this.firstName + " " + this.lastName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<>();
        if (StringUtils.isBlank(firstName)) {
            errors.add(new ValidationError("firstName", "error.required"));
        }

        if (StringUtils.isBlank(lastName)) {
            errors.add(new ValidationError("lastName", "error.required"));
        }

        if (!StringUtils.isEmpty(this.getPassword()) && !Pattern.compile(PASSWORD_PATTERN).matcher(this.getPassword()).find()) {
            errors.add(new ValidationError("password", "auth.login.password.pattern.not.match"));
        }

        return errors.isEmpty() ? null : errors;
    }
}
