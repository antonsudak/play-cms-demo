package util;


import akka.actor.ActorSystem;
import blocks.pageblock.DefaultPageBlock;
import blocks.teaserblock.TeaserBlock;
import ch.insign.cms.CMSApi;
import ch.insign.cms.CMSApiLifecycleImpl;
import ch.insign.cms.blocks.backendlinkblock.BackendLinkBlock;
import ch.insign.cms.blocks.errorblock.ErrorPage;
import ch.insign.cms.blocks.groupingblock.GroupingBlock;
import ch.insign.cms.blocks.horizontalcollection.HorizontalCollectionBlock;
import ch.insign.cms.blocks.jotformpageblock.JotFormPageBlock;
import ch.insign.cms.blocks.linkblock.LinkBlock;
import ch.insign.cms.blocks.searchresultblock.SearchResultBlock;
import ch.insign.cms.blocks.sliderblock.SliderCollectionBlock;
import ch.insign.cms.models.CollectionBlock;
import ch.insign.cms.models.ContentBlock;
import crud.page.CarInventoryPage;
import play.db.jpa.JPAApi;
import widgets.registeredusers.RegisteredUsersWidget;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DemoProjectCmsApiLifecycle extends CMSApiLifecycleImpl {

    @Inject
    public DemoProjectCmsApiLifecycle(
            JPAApi jpaApi,
            ActorSystem actorSystem
    ) {
        super(jpaApi, actorSystem);
    }

    @Override
    protected void registerCmsBlocks(CMSApi cmsApi) {

        cmsApi.getBlockManager().register(
                // base cms blocks except PageBlock
                ContentBlock.class,
                CollectionBlock.class,
                LinkBlock.class,
                BackendLinkBlock.class,
                GroupingBlock.class,
                SliderCollectionBlock.class,
                SearchResultBlock.class,
                JotFormPageBlock.class,
                ErrorPage.class,
                HorizontalCollectionBlock.class,
                // project specific blocks
                DefaultPageBlock.class,
                TeaserBlock.class,
                CarInventoryPage.class
        );
    }

    @Override
    protected void registerContentFilters(CMSApi cmsApi) {
        super.registerContentFilters(cmsApi);
        cmsApi.getFilterManager().register(new RegisteredUsersWidget());
    }

    @Override
    protected void registerUncachedPartials(CMSApi cmsApi) {
        super.registerUncachedPartials(cmsApi);

        cmsApi.getUncachedManager()
                .register("flashPartial", () -> ch.insign.cms.views.html.admin.shared.flashPartial.render());
    }
}
