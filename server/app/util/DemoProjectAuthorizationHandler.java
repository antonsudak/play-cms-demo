package util;

import ch.insign.cms.security.CmsAuthorizationHandler;
import ch.insign.playauth.authz.AuthorizationHandler;
import org.apache.shiro.authz.AuthorizationException;
import play.db.jpa.JPAApi;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;

public class DemoProjectAuthorizationHandler extends CmsAuthorizationHandler implements AuthorizationHandler {

    @Inject
    public DemoProjectAuthorizationHandler(JPAApi jpaApi) {
        super(jpaApi);
    }

    @Override
    public Result onUnauthorized(Http.Context ctx, AuthorizationException e) {
        return super.onUnauthorized(ctx, e);
    }

    @Override
    public Result onUnauthenticated(Http.Context ctx, AuthorizationException e) {
        return super.onUnauthenticated(ctx, e);
    }

}
