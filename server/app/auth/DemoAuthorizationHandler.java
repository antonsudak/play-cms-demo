package auth;

import ch.insign.cms.security.CmsAuthorizationHandler;
import org.apache.shiro.authz.AuthorizationException;
import play.db.jpa.JPAApi;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;

public class DemoAuthorizationHandler extends CmsAuthorizationHandler implements ch.insign.playauth.authz.AuthorizationHandler {

    @Inject
    public DemoAuthorizationHandler(JPAApi jpaApi) {
        super(jpaApi);
    }

    @Override
    public Result onUnauthorized(Http.Context ctx, AuthorizationException e) {
        return super.onUnauthorized(ctx, e);
    }

    @Override
    public Result onUnauthenticated(Http.Context ctx, AuthorizationException e) {
        return super.onUnauthenticated(ctx, e);
    }
}
