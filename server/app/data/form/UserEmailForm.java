package data.form;

public interface UserEmailForm {
    String getEmail();
}
