package data.form;

import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static data.form.RegisterUserForm.PASSWORD_PATTERN;

public class ResetPasswordForm {

    @Constraints.Required
    private String token;

    @Constraints.Required
    private String password;

    @Constraints.Required
    private String passwordRepeat;

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<>();

        if(!password.equals(passwordRepeat)) {
            errors.add(new ValidationError("passwordRepeat", "auth.password.signup.error.passwords_not_same"));
        }

        if (null == password || !Pattern.compile(PASSWORD_PATTERN).matcher(password).find()) {
            errors.add(new ValidationError("password", "auth.login.password.pattern.not.match"));
        }

        return errors.isEmpty() ? null : errors;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordRepeat() {
        return passwordRepeat;
    }

    public void setPasswordRepeat(String passwordRepeat) {
        this.passwordRepeat = passwordRepeat;
    }
}
