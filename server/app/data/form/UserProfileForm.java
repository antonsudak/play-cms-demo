package data.form;

import ch.insign.cms.models.CMS;
import ch.insign.playauth.party.ISOGender;
import org.apache.commons.lang3.StringUtils;
import play.data.validation.Constraints;
import play.data.validation.Constraints.Required;
import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class UserProfileForm implements UserEmailForm {
    public static final String PHONE_PATTERN = "^\\d{6,11}$";

    @Required
    @Constraints.Email
    private String email;

    @Required
    private String password;

    @Required
    private ISOGender gender;

    @Required
    @Constraints.MaxLength(25)
    private String firstName;

    @Required
    @Constraints.MaxLength(30)
    private String lastName;

    @Constraints.MaxLength(11)
    private String phone;

    private String language;

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<>();

        if (!StringUtils.isBlank(phone) && !Pattern.compile(PHONE_PATTERN).matcher(phone).find()) {
            errors.add(new ValidationError("phone", "user.register.phone.wrong.format"));
        }

        if (!StringUtils.isBlank(language) && notMatchAnyFrontendLanguage()) {
            errors.add(new ValidationError("language", "user.register.language.wrong"));
        }

        return errors.isEmpty() ? null : errors;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ISOGender getGender() {
        return gender;
    }

    public void setGender(ISOGender gender) {
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    private boolean notMatchAnyFrontendLanguage(){
        String lang = CMS.getConfig().frontendLanguages()
                .stream()
                .filter(l -> l.equals(language))
                .findFirst()
                .orElse(null);
        if (lang == null) return true;
        else return false;
    }
}
