package data.form;

import ch.insign.playauth.party.ISOGender;
import org.apache.commons.lang3.StringUtils;
import play.data.validation.Constraints;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;
import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Form to register a new user
 */
public class RegisterUserForm implements UserEmailForm {

    /**
     * Password validation regex pattern
     *
     *  - must contains one digit from 0-9
     *  - must contains one lowercase or uppercase characters
     *  - length at least 6 characters
     */
    public static final String PASSWORD_PATTERN = "^((?=.*\\d)(?=.*[a-zA-Z])).{6,}$";
    /**
     * Phone validation regex pattern
     *
     *  - must contain from 6 to 11 characters, only digits
     */
    public static final String PHONE_PATTERN = "^\\d{6,11}$";

    @Constraints.Required
    @Constraints.Email
    private String email;

    @Required
    private ISOGender gender;

    @Required
    @Constraints.MaxLength(25)
    private String firstName;

    @Required
    @Constraints.MaxLength(30)
    private String lastName;

    @Required
    @MinLength(value = 6, message = "constraint.minLength.value-6")
    private String password;

    @Required
    private String passwordRepeat;

    @Constraints.MaxLength(11)
    private String phone;

    private String language;

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<>();

        if(!password.equals(passwordRepeat)) {
            errors.add(new ValidationError("passwordRepeat", "auth.password.signup.error.passwords_not_same"));
        }

        if (null == password || !Pattern.compile(PASSWORD_PATTERN).matcher(password).find()) {
            errors.add(new ValidationError("password", "auth.login.password.pattern.not.match"));
        }

        if (!StringUtils.isBlank(phone) && !Pattern.compile(PHONE_PATTERN).matcher(phone).find()) {
            errors.add(new ValidationError("phone", "user.register.phone.wrong.format"));
        }

        return errors.isEmpty() ? null : errors;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ISOGender getGender() {
        return gender;
    }

    public void setGender(ISOGender gender) {
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordRepeat() {
        return passwordRepeat;
    }

    public void setPasswordRepeat(String passwordRepeat) {
        this.passwordRepeat = passwordRepeat;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
