package data.validator;

import ch.insign.cms.validator.FormValidator;
import ch.insign.playauth.PlayAuthApi;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyManager;
import data.form.UserEmailForm;
import play.data.validation.ValidationError;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserEmailValidator<T extends UserEmailForm> extends FormValidator<T> {

    private final PartyManager partyManager;
    private final PlayAuthApi playAuthApi;

    @Inject
    public UserEmailValidator(PartyManager partyManager, PlayAuthApi playAuthApi) {
        this.partyManager = partyManager;
        this.playAuthApi = playAuthApi;
    }

    @Override
    protected List<ValidationError> validate(T form) {
        List<ValidationError> errors = new ArrayList<>();

        Optional.ofNullable(partyManager.findOneByPrincipal(form.getEmail()))
            .ifPresent(p -> {
                Optional<Party> maybeCurrentParty = playAuthApi.getCurrentParty()
                    .filter(c -> c.getId().equals(p.getId()));

                if (!maybeCurrentParty.isPresent()) {
                    errors.add(new ValidationError("email", "error.user.email_exists"));
                }
            });

        return errors;
    }

}
