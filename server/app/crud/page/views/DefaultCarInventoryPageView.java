package crud.page.views;

import crud.page.CarInventoryPage;
import crud.data.repository.CarRepository;
import play.twirl.api.Html;

public class DefaultCarInventoryPageView implements CarInventoryPageView {

    private CarInventoryPage page;
    private CarRepository repository;

    @Override
    public CarInventoryPageView setPage(CarInventoryPage page) {
        this.page = page;
        return this;
    }

    @Override
    public CarInventoryPageView setRepository(CarRepository repository) {
        this.repository = repository;
        return this;
    }

    @Override
    public Html render() {
        return crud.page.views.html.show.render(page, repository.findAll().getAll().asJava());
    }
}
