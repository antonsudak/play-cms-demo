package crud.data.repository;

import com.uaihebert.factory.EasyCriteriaFactory;
import crud.data.entity.Car;
import crud.util.Paginate;
import io.vavr.control.Either;
import io.vavr.control.Option;
import io.vavr.control.Try;
import play.db.jpa.JPAApi;

import javax.inject.Inject;

public class CarRepository {

    private final JPAApi jpaApi;

    @Inject
    public CarRepository(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public Either<Throwable, Car> save(Car car) {
        return Try.of(() -> jpaApi.em().merge(car)).toEither();
    }

    public Try<Void> delete(Car car) {
        return Try.run(() -> jpaApi.em().remove(car));
    }

    public Option<Car> findOneById(long id) {
        return Try.of(() -> Option.of(jpaApi.em().find(Car.class, id))).getOrElse(Option.none());
    }

    public Option<Car> findOneByRegistrationId(String registrationId) {
        return Try.of(() -> Option.of(jpaApi.em().createNamedQuery("Car.findByRegistrationId", Car.class)
                .setParameter("registrationId", registrationId)
                .getSingleResult())).getOrElse(Option.none());
    }

    public Paginate<Car> findAll() {
        return Paginate.of(EasyCriteriaFactory.createQueryCriteria(jpaApi.em(), Car.class));
    }

    public Paginate<Car> findBySearchTerm(String searchTerm) {
        return Paginate.of(jpaApi.em().createNamedQuery("Car.findBySearchTerm", Car.class)
                    .setParameter("searchTerm1", "%" + searchTerm +"%")
                    .setParameter("searchTerm2", "%" + searchTerm +"%"),
                jpaApi.em().createNamedQuery("Car.findBySearchTerm.count", Long.class)
                    .setParameter("searchTerm1", "%" + searchTerm +"%")
                    .setParameter("searchTerm2", "%" + searchTerm +"%")
        );
    }

    public Paginate<Car> findByBrandIdAndSearchTerm(long brandId, String searchTerm) {
        return Paginate.of(jpaApi.em().createNamedQuery("Car.findByBrandIdAndSearchTerm", Car.class)
                    .setParameter("searchTerm1", "%" + searchTerm +"%")
                    .setParameter("searchTerm2", "%" + searchTerm +"%")
                    .setParameter("brandId", brandId),
                jpaApi.em().createNamedQuery("Car.findByBrandIdAndSearchTerm.count", Long.class)
                    .setParameter("searchTerm1", "%" + searchTerm +"%")
                    .setParameter("searchTerm2", "%" + searchTerm +"%")
                    .setParameter("brandId", brandId)
        );
    }

    public Paginate<Car> findByBrandId(long brandId) {
        return Paginate.of(EasyCriteriaFactory.createQueryCriteria(jpaApi.em(), Car.class)
                .leftJoin("brand")
                .andEquals("brand.id", brandId));
    }
}
