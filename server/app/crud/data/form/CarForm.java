package crud.data.form;

import crud.data.entity.Brand;
import play.data.validation.Constraints;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.util.Date;

public class CarForm {

    private Long id;

    @Constraints.Required
    private Brand brand;

    @Constraints.Required
    @Constraints.MaxLength(value = 48)
    private String model;

    @Constraints.Required
    @Constraints.MaxLength(value = 10)
    private String registrationId;

    @Constraints.Required
    @Constraints.Min(value = 0, message = "example.crud.car.price.error.message")
    @Digits(integer = 10, fraction = 2, message = "example.crud.car.price.error.message")
    private BigDecimal price;

    private Date buyDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(Date buyDate) {
        this.buyDate = buyDate;
    }
}
