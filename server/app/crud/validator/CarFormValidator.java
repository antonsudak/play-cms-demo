package crud.validator;

import ch.insign.cms.validator.FormValidator;
import crud.data.form.CarForm;
import crud.data.repository.CarRepository;
import play.data.validation.ValidationError;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CarFormValidator extends FormValidator<CarForm> {

    private CarRepository carRepository;

    @Inject
    public CarFormValidator(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Override
    protected List<ValidationError> validate(CarForm form) {
        List<ValidationError> errors = new ArrayList<>();

        carRepository.findOneByRegistrationId(form.getRegistrationId())
                .filter(car -> !car.getId().equals(form.getId()))
                .peek(car -> errors.add(new ValidationError("registrationId", "example.crud.car.registrationId.nonunique")));

        if (form.getBuyDate() != null) {
            if (form.getBuyDate().before(new Date(0))) {
                errors.add(new ValidationError("buyDate", "example.crud.car.date.error"));
            }
        }

        return errors;
    }
}
