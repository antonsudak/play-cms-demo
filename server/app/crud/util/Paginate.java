package crud.util;

import com.uaihebert.model.EasyCriteria;
import io.vavr.Function0;
import io.vavr.collection.List;
import io.vavr.collection.Stream;
import javax.persistence.TypedQuery;

public class Paginate<T> {

    public static class Page {

        private final int pages;
        private final int page;
        private final long count;
        private final int first;
        private final int last;

        private Page(int pages, int page, long count, int size) {
            this.pages = pages;
            this.page = page;
            this.count = count;
            this.first = (page - 1) * size + 1;
            this.last = (count < page * size) ? (int)count : page * size;
        }

        public int getPages() {
            return pages;
        }

        public int getPage() {
            return page;
        }

        public long getCount() {
            return count;
        }

        public int getFirst() {
            return first;
        }

        public int getLast() {
            return last;
        }

    }

    public static final int DEFAULT_PAGE_SIZE = 50;
    public static final List<Integer> PAGE_SIZE_LIST = List.of(20, 50, 100);

    public static <T> Paginate<T> of(EasyCriteria<T> query) {
        return new Paginate<>(query);
    }

    public static <T> Paginate<T> of(TypedQuery<T> typedQuery, TypedQuery<Long> totalCount) {
        return new Paginate<>(typedQuery, totalCount);
    }

    private  EasyCriteria<T> query;
    private final Function0<Long> count;
    private final int pageSize;
    private TypedQuery<T> typedQuery;

    private Paginate(EasyCriteria<T> query) {
        this.query = query;
        this.count = query::count;
        this.pageSize = DEFAULT_PAGE_SIZE;
    }

    private Paginate(EasyCriteria<T> query, Function0<Long> count, int pageSize) {
        this.query = query;
        this.count = count;
        this.pageSize = pageSize;
    }

    /**
     * Create a new paginate for TypedQuery source.
     * Hint: Do not use TypedQuery if you can use EasyCriteria.
     */

    private Paginate(TypedQuery<T> typedQuery, TypedQuery<Long> totalCount) {
        this.typedQuery = typedQuery;
        this.count = (() -> totalCount.getSingleResult());
        this.pageSize = DEFAULT_PAGE_SIZE;

    }

    private Paginate(TypedQuery<T> typedQuery, Function0<Long> count, int pageSize) {
        this.typedQuery = typedQuery;
        this.count = count;
        this.pageSize = pageSize;
    }


    public Paginate<T> withPageSize(int pageSize) {
        if (query != null) return new Paginate<>(query, count, pageSize);
        else return new Paginate<>(typedQuery, count, pageSize);
    }

    public Paginate<T> withCount(Function0<Long> count) {
        return new Paginate<>(query, count, pageSize);
    }

    public Paginate<T> withCount(long count) {
        return new Paginate<>(query, () -> count, pageSize);
    }

    public Stream<T> getPage(int page) {
        if (query != null) return Stream.ofAll(query.setFirstResult((page - 1) * pageSize).setMaxResults(pageSize).getResultList());
        else return Stream.ofAll(typedQuery.setFirstResult((page - 1) * pageSize).setMaxResults(pageSize).getResultList());
    }

    /**
     * Use with caution. Will fetch all the entities from the database and evaluate (i.e. fetch all linked entities)
     * them, potentially causing high CPU and memory load.
     *
     * @return Stream of all entities in the provided EasyCriteria query
     */
    public Stream<T> getAll() {
        return Stream.ofAll(query.setFirstResult(0).setMaxResults((int)getCount()).getResultList());
    }

    public Page getPageInfo(int page) {
        return new Page(getPages(), page, getCount(), getPageSize());
    }

    /**
     * Calling this method will evaluate the Stream size by default. Only do this if you're sure it's a finite (and not
     * too large) stream, or if `withCount` was called and evaluated beforehand.
     *
     * @return Amount of pages
     */
    public int getPages() {
        return (int) Math.ceil((double) getCount() / getPageSize());
    }

    public long getCount() {
        return count.get();
    }

    public int getPageSize() {
        return pageSize;
    }

    public boolean isEmpty() {
        return !nonEmpty();
    }

    public boolean nonEmpty() {
        return getCount() > 0;
    }

}
