# Play-CMS Demo #

This is a Play framework demo application built with [insign](http://www.insign.ch)s **play-cms** - a java-based Play
framework CMS.

This demo creates some demo content and uses the h2 in-memory db for storage. It is intended to be used as
quick-and-easy starting point for your own play-cms project. We've also added a docker-based devops setup for a CI
(continuous integration) process.

Check out the [play-cms](http://www.play-cms.com) website for more information and documentation or visit the
[play-cms community](https://plus.google.com/u/0/communities/100224277865065215900/)

## Quick start

Either do a local installation (if you have the play framework installed) using H2 by default, or run it in Docker which
uses MySQL database by default.

### Local installation ###

Clone the project to your computer, navigate to the new folder and run

    sbt run

By default, the application will be running on [http://localhost:9000/]() and use a H2 in-memory db for persistence.
After starting up the application, you need to execute the bootstrap code by calling [http://localhost:9000/reset](),
otherwise you will get an error.

### Backend Access ###

Default admin page url: [http://localhost:9000/admin]()

#### Credentials ####

- Login: admin@insign.ch
- Password: temp123

## Development environment and Continuous Integration ##

The project contains both a development environment and continuous integration setup (based on docker) for quick 
starting new projects. 

### Use the development environment ###

- Make sure you have `docker` and `docker-compose` installed
- The `services.sh` is a helper script which initializes required environment variables and forwards a passed command to
either `docker-compose` or `sbt` commands:
   - `services.sh [docker-compose arguments...]` - run full docker stack
   - `services.sh sbt [sbt arguments...]` - run `sbt` command along with all docker containers except `app` container
- The bound tcp ports at `services.yml` and `sbt.yml` are configurable via environment variables which may be overridden
in `./devops/.env` file
- SBT options can be specified in `./.sbtopts` file, but it must be noted that a line feed is required at file end
because otherwise the last option will be ignored by sbt

To quick start the project with this setup, run

    cp devops/.env-example devops/.env
    devops/services.sh sbt
    
and then navigate to [http://localhost/](). Please be aware that on computers using `docker-machine`, you'll have to
change `localhost` with the IP of your Docker machine, in the browser as well as the `.env` file.

The environment features these dockerized applications:

- MySQL with PHPMyAdmin under [http://localhost/~pma/]()
- Mailhog SMTP server and mail viewer under [http://localhost/~mail/]()

As with the setup not using docker, you will have to call the `/reset` route after starting the application for the
first time.

### Developing the play modules ##

If you want to develop the play-* modules, execute the following commands in your project root folder:

```
cp .sbtopts-example .sbtopts
mkdir modules
cd modules
git clone git@bitbucket.org:insigngmbh/play-cms.git
```

### CI setup with Jenkins ###

Jenkins container with already configured job which builds and tests demo application on every commit can be run with: 

    devops/jenkins.sh

Its web interface is exposed on port 8083.

## Developing the demo project itself ##

### Publishing modules to bintray ###

Modules can be published to bintray with

    sbt publish

Credentials will be asked on first time. 

Note: bintray doesn't support snapshots.

Current version of a module can be easily removed from bintray with

    sbt bintrayUnpublish

### Publishing docker image to bintray registry ###

Next command will upload a docker image of play-cms-demo to insign's bintray registry:

    sbt docker:publish
